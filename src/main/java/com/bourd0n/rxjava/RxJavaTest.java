package com.bourd0n.rxjava;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class RxJavaTest {

    public static void main(String[] args) throws InterruptedException {
        Flowable.just("Hello").subscribe(System.out::println);
        Flowable.range(1, 100)
                .map(v -> v * v)
                .filter(v -> v % 3 == 0)
                .subscribe(System.out::println)
                .dispose();

        Observable.create(emitter -> {
            while (!emitter.isDisposed()) {
                long time = System.currentTimeMillis();
                emitter.onNext(time);
                if (time % 2 == 0) {
                    emitter.onError(new IllegalAccessException("Odd milliseconds"));
                }
            }
        }).subscribe(System.out::println, Throwable::printStackTrace);

        Flowable.fromCallable(() -> {
            System.out.println("Start computation");
            Thread.sleep(5000);
            return "Done";
        })
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.single())
                .subscribe(System.out::println, System.err::println);

        System.out.println("Wait");
//        Thread.sleep(10000);


        Flowable.range(1, 100)
                .publish()
                .parallel()
                .runOn(Schedulers.computation())
                .map(v -> v * v)
                .sequential()
                .subscribe(System.out::println, System.err::println);
    }
}
